﻿namespace 貪吃蛇
{
    partial class Door
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.buttonEasy = new System.Windows.Forms.Button();
            this.buttonNormal = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.buttonTime = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            this.buttonFraction = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("新細明體", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(109, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "貪食蛇";
            // 
            // buttonEasy
            // 
            this.buttonEasy.ForeColor = System.Drawing.Color.Green;
            this.buttonEasy.Location = new System.Drawing.Point(55, 107);
            this.buttonEasy.Name = "buttonEasy";
            this.buttonEasy.Size = new System.Drawing.Size(220, 47);
            this.buttonEasy.TabIndex = 1;
            this.buttonEasy.Text = "簡單";
            this.buttonEasy.UseVisualStyleBackColor = true;
            this.buttonEasy.Click += new System.EventHandler(this.buttonEasy_Click);
            // 
            // buttonNormal
            // 
            this.buttonNormal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.buttonNormal.Location = new System.Drawing.Point(55, 191);
            this.buttonNormal.Name = "buttonNormal";
            this.buttonNormal.Size = new System.Drawing.Size(220, 47);
            this.buttonNormal.TabIndex = 2;
            this.buttonNormal.Text = "普通";
            this.buttonNormal.UseVisualStyleBackColor = true;
            this.buttonNormal.Click += new System.EventHandler(this.buttonNormal_Click);
            // 
            // button2
            // 
            this.button2.ForeColor = System.Drawing.Color.Red;
            this.button2.Location = new System.Drawing.Point(58, 275);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(220, 47);
            this.button2.TabIndex = 3;
            this.button2.Text = "困難";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // buttonTime
            // 
            this.buttonTime.ForeColor = System.Drawing.Color.Purple;
            this.buttonTime.Location = new System.Drawing.Point(58, 359);
            this.buttonTime.Name = "buttonTime";
            this.buttonTime.Size = new System.Drawing.Size(220, 47);
            this.buttonTime.TabIndex = 4;
            this.buttonTime.Text = "越來越快";
            this.buttonTime.UseVisualStyleBackColor = true;
            this.buttonTime.Click += new System.EventHandler(this.buttonTime_Click);
            // 
            // buttonExit
            // 
            this.buttonExit.Location = new System.Drawing.Point(55, 527);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(220, 47);
            this.buttonExit.TabIndex = 5;
            this.buttonExit.Text = "離開";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // buttonFraction
            // 
            this.buttonFraction.Location = new System.Drawing.Point(55, 443);
            this.buttonFraction.Name = "buttonFraction";
            this.buttonFraction.Size = new System.Drawing.Size(220, 47);
            this.buttonFraction.TabIndex = 6;
            this.buttonFraction.Text = "分數";
            this.buttonFraction.UseVisualStyleBackColor = true;
            this.buttonFraction.Click += new System.EventHandler(this.buttonFraction_Click);
            // 
            // Door
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lime;
            this.ClientSize = new System.Drawing.Size(330, 607);
            this.Controls.Add(this.buttonFraction);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.buttonTime);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.buttonNormal);
            this.Controls.Add(this.buttonEasy);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "Door";
            this.Text = "入口";
            this.Load += new System.EventHandler(this.Door_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonEasy;
        private System.Windows.Forms.Button buttonNormal;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button buttonTime;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Button buttonFraction;
    }
}