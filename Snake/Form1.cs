﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 貪吃蛇
{
    public partial class Form1 : Form
    {
        PictureBox px = new PictureBox();
        PictureBox px2 = new PictureBox();
        PictureBox[] snake = new PictureBox[1];
        PictureBox[] snake2 = new PictureBox[9999];

        public static int[] Friction = new int[]{0,0,0,0,0,0};
        public static string[] name = new string[6];
        public static string Ranking;


        int i = 1;
       

        public Form1()
        {
            InitializeComponent();
        }

        Random rd = new Random();
        int x, y,x1,y1;
        int[] x2 = new int[9999];
        int[] y2 = new int[9999];

        public void eat()
        {
            x = rd.Next(1, 450);
            y = rd.Next(1, 450);
            px.Size = new Size(30, 30);
            px.Location = new Point(x, y);
            px.BackColor = Color.Yellow;
            px.Name = "eat";           
            this.Controls.Add(px);                   

        }
       
        public void Snake()
        {
            PictureBox px2 = new PictureBox();

           

            px2.Size = new Size(30, 30);
            px2.Location = new Point(x1, y1);
            px2.BackColor = Color.Lime;
            px2.Name = "snake" + i;

            Array.Resize(ref snake, i+1);

            snake[i] = px2;

            this.Controls.Add(snake[i]);
           
            i++;
           
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            timer1.Interval = Door.speed;
            timer2.Enabled = Door.timeSpeed;
            snake[0] = pictureBox1;
            eat();
            labelSpeed.Text = Door.Difficulty;
            
        }

        int move = 1;

        private void timer1_Tick(object sender, EventArgs e)
        {
            for (int i = 1; i < snake.Length; i++)
            {
                if (snake[0].Top > snake[i].Top - 30 && snake[0].Top < snake[i].Top + 30)
                {
                    if (snake[0].Left > snake[i].Left - 30 && snake[0].Left < snake[i].Left + 30)
                    {
                        timer1.Enabled = false;
                        MessageBox.Show("你掛了,成績是"+label2.Text+"分");
               

                        if (int.Parse(label2.Text) > Friction[5])
                        {
                            Friction[5] = int.Parse(label2.Text);
                            name[5] = Microsoft.VisualBasic.Interaction.InputBox("請輸入大名", "恭喜入榜");
                        }
                      
                        Array.Sort(Friction, name);
                        Array.Reverse(Friction);
                        Array.Reverse(name);
                        Ranking="   排行榜\n";
                        for (int b = 0; b < 5; b++)
                        {
                            Ranking += (b + 1) + ":" + name[b] +"\t"+ Friction[b] + "分\n";
                        }

                            MessageBox.Show(Ranking);

                            this.Dispose();
                        Door dr = new Door();
                        dr.Visible = true;
                    }                                                          
                }
            }
           
            x1 = snake[0].Left;
            y1 = snake[0].Top;
            for (int a = 0; a < snake.Length; a++)
            {
                x2[a] = snake[a].Left;
                y2[a] = snake[a].Top;
            }

            if (move == 1)
            {

                snake[0].Left += sppdUp;
                
                if (snake[0].Left > this.Width -30)   //要是遇到牆壁的處理
                {
                    snake[0].Left = 0;
                }

                for (i = 1; i < snake.Length; i++)    //檢查所有的身體
                {                   
                    snake[i].Location = new Point(x2[i - 1], y2[i - 1]);
                }  
                          
            }

            if (move == 2)
            {

                snake[0].Left -= sppdUp;
                if (snake[0].Left < 0)
                {
                    snake[0].Left = this.Width-45;
                }
                for (i = 1; i < snake.Length; i++)
                {                                  
                    snake[i].Location = new Point(x2[i - 1], y2[i - 1]); 
                }
               
            }

            if (move == 3)
            {

                snake[0].Top += sppdUp;
                if (snake[0].Top > this.Height - 30)
                {
                    snake[0].Top -= snake[0].Top;
                }
                for (i = 1; i < snake.Length; i++)
                {                    
                    snake[i].Location = new Point(x2[i - 1], y2[i - 1]);                        
                }
               
            }

            if (move == 4)
            {

                snake[0].Top -= sppdUp;
                if (snake[0].Top < 0)
                {
                    snake[0].Top = this.Height - 30;
                }
                for (i = 1; i < snake.Length; i++)
                {                                      
                    snake[i].Location = new Point(x2[i - 1], y2[i - 1]);          
                }
               
            }

            if (snake[0].Left > x - 30 && snake[0].Left < x + 30)
            {
                if (snake[0].Top > y - 30 && snake[0].Top < y + 30)
                {
                    label2.Text = (int.Parse(label2.Text) + 10).ToString();
                    eat();
                    Snake();
                }
            }                            
        }     

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Down || e.KeyCode == Keys.S) && move != 4)
            {
                move = 3;                 
            }
            if ((e.KeyCode == Keys.Up || e.KeyCode == Keys.W) && move != 3)
            {
                 move=4;                
            }
            if ((e.KeyCode == Keys.Left || e.KeyCode == Keys.A) && move != 1)
            {
                 move=2;
            }
            if ((e.KeyCode == Keys.Right || e.KeyCode == Keys.D) && move != 2)
            {
                move = 1;
            }
        }
        int sppdUp = 30;
        int time = 0;
       
        private void timer2_Tick_1(object sender, EventArgs e)
        {
            time++;
            if (time == 5)
            {


                time = 0;
                labelSpeed.Text = (int.Parse(labelSpeed.Text) + 1).ToString();
                timer1.Interval -= 10;
                if (timer1.Interval == 10)
                {
                    timer2.Enabled = false;
                }
            }
        }

       
    }
}
