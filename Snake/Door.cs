﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.Devices;

namespace 貪吃蛇
{
    public partial class Door : Form
    {
        public Door()
        {
            InitializeComponent();
        }
        public static int speed = 100;
        public static bool timeSpeed = false;
        public static string Difficulty;
        Computer myComputer = new Computer();


        private void Door_Load(object sender, EventArgs e)
        {
            myComputer.Audio.Stop();
        }
        Form1 f1 = new Form1();

        private void buttonEasy_Click(object sender, EventArgs e)
        {
            speed = 100;
            Difficulty = "簡單";
            f1.Show();
            this.Hide();
            

        }

        private void buttonNormal_Click(object sender, EventArgs e)
        {
            speed = 50;
            Difficulty = "普通";
            f1.Show();
            this.Hide();
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            speed = 20;
            Difficulty = "困難";
            f1.Show();
            this.Hide();
            
        }

        private void buttonTime_Click(object sender, EventArgs e)
        {
            timeSpeed = true;
            Difficulty = "1";
            myComputer.Audio.Play("123.wav", AudioPlayMode.Background);
            f1.Show();
            this.Hide();
           
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonFraction_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Form1.Ranking);
        }
    }
}
